package rojas.juan.ucenfotec;
import rojas.juan.ucenfotec.tl.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;


public class Main {

    public static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    public static PrintStream out = System.out;
    public static Controller gestor = new Controller();
    public static ControllerSintoma cs = new ControllerSintoma();
    public static ControllerMedico cm = new ControllerMedico();
    public static ControllerFactorRiesgo cfr= new ControllerFactorRiesgo();
    public static ControllerEnfermedad ce = new ControllerEnfermedad();
    public static ControllerCentroMedico ccm = new ControllerCentroMedico();

    public static void main (String[] args) throws java.io.IOException {
        boolean salir = true;
        try {
            do {
                mostrarMenu();
                int opcion = pedirEntero();
                salir = ejecutar(opcion);

            } while (salir == false);
        } catch (Exception e) {
            System.out.println("Solo se permiten numeros, vuelvalo a intentar.");
        }
    }

    /**
     *
     * @return
     * @throws IOException
     */
    public static int pedirEntero() throws IOException {
        int opcion = 0;

        out.println("Elija una opcion: ");
        opcion = Integer.parseInt((in.readLine()));

        return opcion;
    }


    /**
     * Menú
     */
    public static void mostrarMenu() {
        out.println("==========MENU PRINCIPAL=============");
        out.println("");
        out.println("1.Registrar usuario.");
        out.println("2.Listar médicos.");
        out.println("3.Listar pacientes.");
        out.println("4.Registrar sintoma.");
        out.println("5.Listar síntomas.");
        out.println("6.Registrar factor de riesgo.");
        out.println("7.Listar factor de riesgo.");
        out.println("8.Registrar enfermedad.");
        out.println("9.Listar enfermedad.");
        out.println("10.Salir.");
        out.println("-------------------------------------");

    }// fin del mostrarMenu

    public static boolean ejecutar(int opcion) throws Exception {
        boolean salir = false;

        switch (opcion) {
            case 1:
                registrarUsuario();
                break;
            case 2:
                listarMedicos();
                break;
            case 3:
                listarPacientes();
                break;
            case 4:
                registrarSintoma();
                break;
            case 5:
                listarSintomas();
                break;
            case 6:
                registrarFactorRiesgo();
                break;
            case 7:
                listarFactorRiesgo();
                break;
            case 8:
                registrarEnfermedad();
                break;
            case 9:
                listarEnfermedades();
                break;
            case 10:
                salir = true;
                break;
            default:
                System.out.println("Opcion invalida");
                break;
        } // fin del switch

        return salir;
    } // fin de ejecutar

    /**
     *
     * @throws Exception
     */
    public static void registrarUsuario() throws Exception {

        out.println("Digite 1 para registrar un Medico, 2 para registrar un paciente sospechoso:");
        int usuario = Integer.parseInt(in.readLine());
        if(usuario == 1){
            medico();
        } else {
            if (usuario==2){
                sospechoso();
            }
        }

    }

    /**
     *
     * @throws IOException
     * metodo para insertar medico en base de datos
     */
    public static void medico() throws IOException {
        String nombre;
        int edad;
        char genero;
        String tipoID;
        String ID;
        String provincia;
        String canton;
        String distrito;
        int tamNucleoF;
        String codColegioMed;
        String especialidad;
        out.println("Digite el nombre del medico:");
        nombre = in.readLine();
        out.println("Digite la edad:");
        edad = Integer.parseInt(in.readLine());
        out.println("Digite el género:");
        genero = in.readLine().charAt(0);
        out.println("Digite el tipo de identificación:");
        tipoID = in.readLine();
        out.println("Digite el número de identificación:");
        ID = in.readLine();
        out.println("Digite la provincia:");
        provincia = in.readLine();
        out.println("Digite el cantón:");
        canton = in.readLine();
        out.println("Digite el distrito:");
        distrito = in.readLine();
        out.println("Digite el tamano de núcleo familiar:");
        tamNucleoF = in.readLine().charAt(0);
        out.println("Digite el código del colegio de médicos:");
        codColegioMed = in.readLine();
        out.println("Digite la especialidad:");
        especialidad = in.readLine();
        out.println("Digite el centro médico donde trabaja::");
        String nombreC = in.readLine();
        out.println("Digite la provincia del centro medico:");
        String provinciaC = in.readLine();
        out.println("Digite el cantón del centro médico:");
        String cantonC = in.readLine();
        out.println("Digite el distrito del centro médico:");
        String distritoC = in.readLine();
        out.println("Digite la capacidad del centro médico.");
        int capacidad = in.readLine().charAt(0);
        System.out.println(cm.insertarMedico(nombre, edad, genero, tipoID, ID, provincia, canton, distrito, tamNucleoF, codColegioMed, especialidad, nombreC, provinciaC, cantonC, distritoC, capacidad));
    }

    /**
     * metodo para ingresar paciente sospechoso
     * @throws Exception
     */
    public static void sospechoso() throws Exception {
        String nombre;
        int edad;
        char genero;
        String tipoID;
        String ID;
        String provincia;
        String canton;
        String distrito;
        int tamNucleoF;
        out.println("Digite el nombre del paciente:");
        nombre = in.readLine();
        out.println("Digite la edad:");
        edad = Integer.parseInt(in.readLine());
        out.println("Digite el género:");
        genero = in.readLine().charAt(0);
        out.println("Digite el tipo de identificación:");
        tipoID = in.readLine();
        out.println("Digite el número de identificación:");
        ID = in.readLine();
        out.println("Digite la provincia:");
        provincia = in.readLine();
        out.println("Digite el cantón:");
        canton = in.readLine();
        out.println("Digite el distrito:");
        distrito = in.readLine();
        out.println("Digite el tamano de núcleo familiar:");
        tamNucleoF = in.readLine().charAt(0);
        out.println("Digite el centro médico donde se le atendió::");
        String nombreC = in.readLine();
        out.println("Digite la provincia del centro medico:");
        String provinciaC = in.readLine();
        out.println("Digite el cantón del centro médico:");
        String cantonC = in.readLine();
        out.println("Digite el distrito del centro médico:");
        String distritoC = in.readLine();
        out.println("Digite la capacidad del centro médico.");
        int capacidad = in.readLine().charAt(0);
        gestor.crearSospechoso(nombre, edad, genero, tipoID, ID, provincia, canton, distrito, tamNucleoF, nombreC, provinciaC, cantonC, distritoC, capacidad);
    }

    /**
     * metodo para listar medicos guardados en base de datos
     */
    public static void listarMedicos(){
        int i =0;
        String[] lista = cm.listarMedicos();
        for (String medico:
             lista) {
            out.println(medico);
            i++;
        }
    }

    /**
     * metodo para listar pacientes
     */
    public static void listarPacientes(){
        int i =0;
        String[] lista = gestor.listarPacientes();
        for (String paciente:
                lista) {
            out.println(paciente);
            i++;
        }
    }

    /**
     * metodo para registrar sintoma en base de datos
     * @throws IOException
     */
    public static void registrarSintoma() throws IOException {
        out.println("Digite el nombre del sintoma:");
        String nombre = in.readLine();
        out.println("Describa el sintoma:");
        String descripcion = in.readLine();
        boolean loTiene = true;
        out.println("Digite la fecha de manifestacion:");
        String fManifestacion = in.readLine();
        cs.insertarSintoma(nombre, descripcion, loTiene, fManifestacion);
    }

    /**
     * metodo para listar sintomas
     */
    public static void listarSintomas(){
        int i =0;
        String[] lista = cs.listarSintomas();
        for (String sintoma:
                lista) {
            out.println(sintoma);
            i++;
        }
    }

    /**
     * metoro para registrar factor de riesgo en BD
     * @throws IOException
     */
    public static void registrarFactorRiesgo() throws IOException {
        out.println("Digite el nombre del factor de riesgo:");
        String nombre = in.readLine();
        out.println("Digite la descripcion:");
        String descripcion = in.readLine();
        boolean loTiene = true;
        cfr.insertarFactorRiesgo(nombre, descripcion, loTiene);
    }

    /**
     * metodo para listar factores de riesgo
     */
    public static void listarFactorRiesgo(){
        int i =0;
        String[] lista = cfr.listarFactoresRiesgo();
        for (String factorRiesgo:
                lista) {
            out.println(factorRiesgo);
            i++;
        }
    }

    /**
     * metodo para registrar enfermedades en BD
     * @throws IOException
     */
    public static void registrarEnfermedad() throws IOException {
        out.println("Digite el nombre del factor de riesgo:");
        String nombre = in.readLine();
        out.println("Digite la descripcion:");
        String descripcion = in.readLine();
        out.println("Digite la tasa de mortalidad:");
        String tasaMortalidad = in.readLine();
        out.println("Digite el RO:");
        int RO = Integer.parseInt(in.readLine());
        out.println("Digite los sintomas:");
        String sintomas = in.readLine();
        ce.insertarEnfermedad(nombre, descripcion,tasaMortalidad, RO, sintomas);
    }

    /**
     * metodo para listar enfermedades
     */
    public static void listarEnfermedades(){
        int i =0;
        String[] lista = ce.listarEnfermedades();
        for (String enfermedad:
                lista) {
            out.println(enfermedad);
            i++;
        }
    }



}

